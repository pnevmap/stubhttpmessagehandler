# README #
**A Stub for HttpMessageHandler for unit testing use.**
 
We setup a HttpResponseMessage for the StubHttpMessageHandler and we inject it into the our HttpClient.

So the method PostAsync (of the HttpClient) returns the set result without making any 3rd party call. (The default HttpResponseMessage result is OK)



```
#!c#

[Test]
public  async Task HttpClient_PostAsync_Test()
{
     // Arrange
     StubHttpMessageHandler mockHandler = new StubHttpMessageHandler();
     HttpClient httpClient = new HttpClient(mockHandler);
     string url = "http://www.example.com/";
     HttpContent someContent = new StringContent("some content");
     // Act
     var response = await httpClient.PostAsync(url, someContent, CancellationToken.None);
     // Assert
     Assert.AreEqual(response, HttpStatusCode.OK);
}
```