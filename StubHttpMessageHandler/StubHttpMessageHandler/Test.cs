﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace StubHttpMessageHandler
{
    [TestFixture]
    public class WeatherServiceClientTest
    {
        [Test]
        public  async Task MyApiClient_PostAsync_Test()
        {
            // Arrange
            StubHttpMessageHandler mockHandler = new StubHttpMessageHandler();
            HttpClient httpClient = new HttpClient(mockHandler);
            string url = "http://www.example.com/";
            HttpContent someContent = new StringContent("some content");
            // Act
            var response = await httpClient.PostAsync(url, someContent, CancellationToken.None);
           // Assert
            Assert.AreEqual(response, HttpStatusCode.OK);
        }
    }
}
